<?php

use app\Main;

require 'vendor/autoload.php';

$main = new Main();
$main->start();