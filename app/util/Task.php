<?php


namespace app\util;

use app\module\controller\AutoTask;
use app\module\model\AutoTaskLog;

class Task
{
    public function run()
    {
        $second = date('s');
        if ($second != 0) return;

        $time = date('H:i');
        $autoTask = new AutoTask();
        $list = $autoTask->getList([]);
        foreach ($list as $value) {
            if ($value['auto_time'] == $time) {
                $task = (object)$value;
                $sign = new Sign($task);
                $data = $sign->sign();
                if (empty($data['code'] ?? 1201)) $data = $sign->signRewards();
                $autoTaskLog = new AutoTaskLog();
                $date = date('Y-m-d H:i:s');
                $data = [
                    'content'     => addslashes(json_encode($data ?? [])),
                    'task_id'     => $task->task_id,
                    'create_time' => $date,
                    'update_time' => $date,
                ];
                $autoTaskLog->insert($data);
                var_dump("task_id" . $task->task_id . "已执行");
            }
        }
    }
}