<?php

namespace app\util;

class ApiReturn
{
    public static function success($data, $message = null): array
    {
        return !empty($data['code']) ? $data : [
            'code' => 200,
            'msg'  => $message ?? 'success',
            'data' => $data,
        ];
    }
}