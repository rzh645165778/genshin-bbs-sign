<?php


namespace app\util;


class SignHeader
{
    private string $cookie = '';
    private string $deviceId = 'DE399CFB4E49DF51';
    private string $host = 'api-takumi.mihoyo.com';
    private string $contentType = 'application/json;charset=utf-8';
    private string $accept = 'application/json, text/plain, */*';
    private string $clientType = '5';
    private string $acceptEncoding = 'gzip, deflate, br';
    private string $appVersion = '2.3.0';
    private string $ds = '';
    private string $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36';

    /**
     * @param string $cookie
     */
    public function setCookie(string $cookie): void
    {
        $this->cookie = $cookie;
    }

    /**
     * @param string $deviceId
     */
    public function setDeviceId(string $deviceId): void
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @param string $contentType
     */
    public function setContentType(string $contentType): void
    {
        $this->contentType = $contentType;
    }

    /**
     * @param string $accept
     */
    public function setAccept(string $accept): void
    {
        $this->accept = $accept;
    }

    /**
     * @param string $clientType
     */
    public function setClientType(string $clientType): void
    {
        $this->clientType = $clientType;
    }

    /**
     * @param string $appVersion
     */
    public function setAppVersion(string $appVersion): void
    {
        $this->appVersion = $appVersion;
    }

    /**
     * @param string $ds
     */
    public function setDs(string $ds): void
    {
        $this->ds = $ds;
    }

    /**
     * @param string $userAgent
     */
    public function setUserAgent(string $userAgent): void
    {
        $this->userAgent = $userAgent;
    }


    public function getHeader()
    {
        return [
            'Cookie'            => $this->cookie,
            'x-rpc-device_id'   => $this->deviceId,
            'Host'              => $this->host,
            'Content-type'      => $this->contentType,
            'Accept'            => $this->accept,
            'x-rpc-client_type' => $this->clientType,
            'x-rpc-app_version' => $this->appVersion,
            'User-Agent'        => $this->userAgent,
            'DS'                => $this->ds,
        ];
    }
}