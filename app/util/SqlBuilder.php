<?php


namespace app\util;


class SqlBuilder
{
    public function where($where): string
    {
        $where_sql = '';
        if (!empty($where)) {
            $where_sql = ' where ';
            foreach ($where as $key => $value) {
                $where_sql .= ($key > 0 ? ' and ' : ' ') . "`{$value[0]}` {$value[1]} '{$value[2]}'";
            }
        }
        return $where_sql;
    }

    public function set($data): string
    {
        $set_sql = '';
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $set_sql .= "`$key` = '$value'" . ($key > 0 ? ',' : '');
            }
        }
        return $set_sql;
    }

    public function insert($data): string
    {
        $insert_sql = '';
        if (!empty($data)) {
            $keys = array_keys($data);
            $values = array_values($data);
            $field = '`' . implode('`,`', $keys) . '`';
            $value = "'" . implode("','", $values) . "'";
            $insert_sql = "($field) values ($value)";
        }
        return $insert_sql;
    }

}