<?php


namespace app\util;

use Firebase\JWT\JWT;

class Token
{
    public function encode(array $info, $key = 'default', $alg = 'HS256'): string
    {
        return JWT::encode($info, $key, $alg);
    }

    public function decode(string $string, $key = 'default', $alg = 'HS256'): array
    {
        try {
            return (array)JWT::decode($string, $key, [$alg]);
        } catch (\Throwable $exception) {
            return [];
        }
    }

}