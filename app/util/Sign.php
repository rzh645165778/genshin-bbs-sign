<?php


namespace app\util;


use Curl\Curl;

class Sign
{
    private object $task;
    private SignHeader $header;
    private string $salt = 'h8w582wxwgqvahcdkpvdhbh2w9casgfl'; //2.3.0 web
    private string $actId = 'e202009291139501';
    private string $nickname = '';
    private string $uid = '';
    private string $region = 'cn_gf01';

    public function __construct(object $task)
    {
        $this->task = $task;
        $this->buildHeader();
    }

    private function buildHeader()
    {
        $signHeader = new SignHeader();
        $signHeader->setCookie($this->task->cookie);
        $signHeader->setDs($this->buildDS());
        $this->header = $signHeader;
    }

    private function buildDS(): string
    {
        $time = time();
        $string = buildRandomString(6);
        $encode = md5("salt=" . $this->salt . "&t=" . $time . "&r=" . $string);
        return implode(',', [$time, $string, $encode]);
    }

    private function setHeader(Curl $curl): Curl
    {
        foreach ($this->header->getHeader() as $key => $value) $curl->setHeader($key, $value);
        return $curl;
    }

    public function getInfo()
    {
        $curl = $this->setHeader(new Curl());
        $data = $curl->get('https://api-takumi.mihoyo.com/binding/api/getUserGameRolesByCookie?game_biz=hk4e_cn');
        $data = $data->response ? json_decode($data->response, true) : [];
        $this->uid = $data['data']['list'][0]['game_uid'] ?? '';
        $this->nickname = $data['data']['list'][0]['nickname'] ?? '';
        return $data;
    }

    public function sign()
    {
        try {
            $this->getInfo();
            $curl = $this->setHeader(new Curl());
            if (empty($this->uid)) return ['code' => -1, 'message' => '获取uid失败'];
            $data = [
                'act_id' => $this->actId,
                'region' => $this->region,
                'uid'    => $this->uid,
            ];
            $data = $curl->post('https://api-takumi.mihoyo.com/event/bbs_sign_reward/sign', $data, true);
            $data = $data->response ? json_decode($data->response, true) : [];
            if ($data['data']['code'] ?? '' === 'ok') return ['message' => '签到成功', 'uid' => $this->uid, 'nickname' => $this->nickname];
        } catch (\Throwable $exception) {
            var_dump($exception->getMessage());
        }
        return ['code' => $data['retcode'] ?? 1021, 'message' => $data['message'] ?? '签到失败'];
    }

    public function signRewards()
    {
        try {
            var_dump(1);
            $this->getInfo();
            $curl = new Curl();
            $data = $curl->get('https://api-takumi.mihoyo.com/event/bbs_sign_reward/home?act_id=' . $this->actId);
            $data = $data->response ? json_decode($data->response, true) : [];
            if (0 === $data['retcode'] ?? 1021) {
                $rewardsList = $data['data']['awards'];
                $signInfo = $this->signInfo(false);
                $day = $signInfo['is_sign'] ? $signInfo['total_sign_day'] - 1 : $signInfo['total_sign_day'];
                return ['rewards' => $rewardsList[$day], 'uid' => $this->uid, 'nickname' => $this->nickname];
            }
        } catch (\Throwable $exception) {
            var_dump($exception->getMessage());
        }
        return ['code' => $data['retcode'] ?? 1021, 'message' => $data['message'] ?? '获取失败'];
    }

    public function signInfo(bool $getInfo = true)
    {
        try {
            $getInfo && $this->getInfo();
            $curl = $this->setHeader(new Curl());
            $data = [
                'act_id' => $this->actId,
                'region' => $this->region,
                'uid'    => $this->uid,
            ];
            $data = $curl->get('https://api-takumi.mihoyo.com/event/bbs_sign_reward/info', $data);
            $data = $data->response ? json_decode($data->response, true) : [];
            if (0 === $data['retcode'] ?? 1021) return ['total_sign_day' => $data['data']['total_sign_day'], 'is_sign' => $data['data']['is_sign']];
        } catch (\Throwable $exception) {
            var_dump($exception->getMessage());
        }
        return ['code' => $data['retcode'] ?? 1021, 'message' => $data['message'] ?? '获取失败'];

    }
}