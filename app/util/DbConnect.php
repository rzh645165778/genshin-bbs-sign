<?php

namespace app\util;
class DbConnect
{
    private $config = [
        'server'   => 'localhost',
        'dbname'   => 'mihoyoSign',
        'username' => 'root',
        'password' => ''
    ];

    /**
     * @param string[] $config
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return string[]
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    public function __construct(array $config = [])
    {
        $config && $this->setConfig($config);
    }

    public function select($sql)
    {
        try {
            $connect = $this->connect();
            $result = $connect->prepare($sql);
            $result->execute();
            return $result->fetchAll(2);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    public function exec($sql)
    {
        try {
            $connect = $this->connect();
            return $connect->exec($sql);
        } catch (\Throwable $exception) {
            return false;
        }

    }

    private function connect(): \PDO
    {
        $config = $this->getConfig();
        return new \PDO("mysql:host={$config['server']};dbname=mihoyoSign;port=3306", $config['username'], $config['password']);
    }


}