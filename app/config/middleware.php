<?php

use app\module\controller\User;
use app\module\controller\AutoTask;
use app\module\middleware\AuthCheck;

return [
    //中间件 => 白名单=>不执行（没有的默认不执行） 黑名单=>执行
    AuthCheck::class => ['black' => [AutoTask::class]]
];