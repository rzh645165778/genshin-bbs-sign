<?php
if (!function_exists('encodePwd')) {
    function encodePwd(string $str, $append = 'gha&G*T&@()!*)(VH@#&%'): string
    {
        return md5(implode(array_reverse(str_split(md5($str) . $append, 2))));
    }
}

if (!function_exists('buildRandomString')) {
    function buildRandomString(int $length): string
    {
        $str = '';
        $strPol = "0123456789abcdefghijklmnopqrstuvwxyz";//大小写字母以及数字
        $max = strlen($strPol) - 1;

        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[rand(0, $max)];
        }
        return $str;
    }
}
