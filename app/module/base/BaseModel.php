<?php


namespace app\module\base;


use app\util\DbConnect;
use app\util\SqlBuilder;

abstract class BaseModel
{
    protected DbConnect $connect;
    protected SqlBuilder $sqlBuilder;
    protected string $class;

    public function __construct()
    {
        $this->connect = new DbConnect();
        $this->sqlBuilder = new SqlBuilder();
    }

    public function get(array $where, string $field = '*')
    {
        $where_sql = $this->sqlBuilder->where($where);
        $sql = "select $field from $this->class $where_sql";
        return $this->connect->select($sql);
    }

    public function insert($data)
    {
        $insert_sql = $this->sqlBuilder->insert($data);
        $sql = "insert into $this->class $insert_sql";
        return $this->connect->exec($sql);
    }

    public function update($where, $data)
    {
        $set_sql = $this->sqlBuilder->set($data);
        $where_sql = $this->sqlBuilder->where($where);
        $sql = "update $this->class set $set_sql $where_sql";
        return $this->connect->exec($sql);
    }


}