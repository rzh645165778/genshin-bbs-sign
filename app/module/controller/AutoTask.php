<?php


namespace app\module\controller;


use app\module\base\BaseController;
use app\module\model\AutoTask as AutoTaskModel;
use app\module\model\AutoTaskLog;
use app\util\Sign;

class AutoTask extends BaseController
{
    protected AutoTaskModel $autoTask;

    public function __construct()
    {
        parent::__construct();
        $this->autoTask = new AutoTaskModel();
    }

    public function add($param, $userInfo)
    {
        $data = [
            'cookie'      => $param['cookie'],
            'user_id'     => $userInfo['id'],
            'auto_time'   => $param['auto_time'],
            'last_time'   => '',
            'create_time' => date('Y-m-d H:i:s'),
        ];
        return $this->autoTask->insert($data);
    }

    public function getList($param, $userInfo = [])
    {
        !empty($userInfo) && $where[] = ['user_id', '=', $userInfo['id']];
        return $this->autoTask->get($where ?? []);
    }

    public function updateCookie($param, $userInfo)
    {
        $where[] = ['task_id', '=', $param['task_id'] ?? 0];
        $where[] = ['user_id', '=', $userInfo['id']];
        $data = ['cookie' => $param['cookie']];
        return $this->autoTask->update($where, $data);
    }

    public function delInfo($param, $userInfo)
    {
        $where[] = ['task_id', '=', $param['task_id'] ?? 0];
        $where[] = ['user_id', '=', $userInfo['id']];
        $data = ['is_del' => 1];
        return $this->autoTask->update($where, $data);
    }
}