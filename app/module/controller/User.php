<?php

namespace app\module\controller;

use app\module\base\BaseController;
use app\module\base\BaseModel;
use app\module\model\AutoUser;
use app\util\Token;

class User extends BaseController
{
    protected AutoUser $autoUser;

    public function __construct()
    {
        parent::__construct();
        $this->autoUser = new AutoUser();
    }

    public function login($param): array
    {
        $where[] = ['username', '=', $param['username']];
        $where[] = ['password', '=', encodePwd($param['password'])];
        $info = $this->autoUser->get($where);
        if (empty($info)) return ['code' => -1, 'msg' => '用户名密码错误'];
        return ['token' => (new Token())->encode($info[0])];
    }

    public function register($param, $user = [])
    {
        $where[] = ['username', '=', $param['username']];
        $info = $this->autoUser->get($where);
        if (!empty($info)) return ['code' => -1, 'msg' => '账号已存在'];

        $data = [
            'username'    => $param['username'],
            'password'    => encodePwd($param['password']),
            'create_time' => date('Y-m-d H:i:s')
        ];
        return $this->autoUser->insert($data);
    }
}