<?php

namespace app\module\model;

use app\module\base\BaseModel;

class AutoTaskLog extends BaseModel
{
    protected string $class = 'auto_task_log';
}