<?php

namespace app\module\model;

use app\module\base\BaseModel;

class AutoUser extends BaseModel
{
    protected string $class = 'auto_user';
}