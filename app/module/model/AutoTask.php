<?php

namespace app\module\model;

use app\module\base\BaseModel;

class AutoTask extends BaseModel
{
    protected string $class = 'auto_task';
}