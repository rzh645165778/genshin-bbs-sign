# genshin-bbs-sign

#### 介绍

原神米游社签到

#### 软件架构

php + swoole扩展

#### 安装教程

1. 下载至本地
2. composer install （ 无composer的请先[安装](https://www.phpcomposer.com) ）

#### 使用说明

1. start.sh 或 php index.php (注意启动代码)

### api文档

☞ [地址](https://www.postman.com/blue-space-873110/workspace/genshin-bbs-sign/documentation/6323665-7248b3cf-bd1c-4920-a72e-28ddb78b20df)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
